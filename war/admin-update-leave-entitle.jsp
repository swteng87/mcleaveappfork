<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
     <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico"/>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      /* 
	Max width before this PARTICULAR table gets nasty
	This query will take effect for any screen smaller than 760px
	and also iPads specifically.
	*/
	@media 
	only screen and (max-width: 760px),
	(min-device-width: 768px) and (max-device-width: 1024px)  {
	
		/* Force table to not be like tables anymore */
		#leaveEntitleList table,
		 #leaveEntitleList thead, 
		 #leaveEntitleList tbody, 
		 #leaveEntitleList th,
		  #leaveEntitleList td, 
		  #leaveEntitleList tr { 
			display: block; 
		}
		
		/* Hide table headers (but not display: none;, for accessibility) */
		#leaveEntitleList thead tr { 
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
		
/* 		#empDetailList tr { border: 1px solid #ccc; } */
		
		#leaveEntitleList td { 
			/* Behave  like a "row" */
/*  		border: none;  */
/* 			border-bottom: 1px solid #eee;  */
			position: relative;
			padding-left: 50%; 
			white-space: normal;
			text-align:left;
		}
		
		#leaveEntitleList td:before { 
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			top: 6px;
			left: 6px;
			width: 45%; 
			padding-right: 10px; 
			white-space: nowrap;
			text-align:left;
		}
		
		/*
		Label the data
		*/
		#leaveEntitleList td:nth-of-type(1):before { content: "Edit"; } 
		#leaveEntitleList td:nth-of-type(2):before { content: "Delete"; } 
		
 		#leaveEntitleList td:nth-of-type(3):before { content: "Region"; }
 		#leaveEntitleList td:nth-of-type(4):before { content: "Entitled Annual"; } 
 		#leaveEntitleList td:nth-of-type(5):before { content: "Hospitalization"; } 
 		
 		#leaveEntitleList td:nth-of-type(6):before { content: "Maternity"; } 
 		#leaveEntitleList td:nth-of-type(7):before { content: "Birthday"; } 
 		#leaveEntitleList td:nth-of-type(8):before { content: "Wedding"; } 
 		#leaveEntitleList td:nth-of-type(9):before { content: "Compassionate"; } 
 		#leaveEntitleList td:nth-of-type(10):before { content: "Compensation"; } 
 		
/* 		#employeeList td:before { content: attr(data-title); } */
	}
	
	/* Smartphones (portrait and landscape) ----------- */
	@media only screen
	and (min-device-width : 320px)
	and (max-device-width : 480px) {
		body { 
			padding: 0; 
			margin: 0; 
			width: 320px; }
		}
	
	/* iPads (portrait and landscape) ----------- */
	@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
		body { 
			width: 495px; 
		}
	}
    </style>
     <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script language="JavaScript">
// 			var running = 0;
			function Go() {
				var f = document.forms[0];
				if( $('input[name=dellist]:checked').length > 0)
				{	        	
					return true;
				}
				else
				{
					$('#myModal').modal('show');
					return false;					
				}
				f.submit();
// 				running++;
			}
// 			function cmd_parm() { 
// 				var f = document.forms[0];
// 				f.submit();
// 			}
  			function clearForm() {
  				var f_elements = document.forms[0].elements;
  				for (i = 0; i < f_elements.length; i++) {
    				field_type = f_elements[i].type.toLowerCase();
    				switch (field_type) {
    					case "text":
    					case "password":
						case "textarea":
    					case "hidden":
    						f_elements[i].value = "";
        					break;
    					case "radio":
    					case "checkbox":
        					if (f_elements[i].checked) {
            					f_elements[i].checked = false;
        					}
        					break;
    					case "select-one":
    						f_elements[i].selectedIndex = 0;
    						break;
    					case "select-multi":
        					f_elements[i].selectedIndex = -1;
        					break;
    					default:
        					break;
    				}
				}
  			}
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
        <script type="text/javascript">
        $(document).ready(function () {
			
				$("#delete-leave-entitle").on("click",function(){
					
					if(Go())
					{
						var dellist = new Array();
						
					$("input[name=dellist]:checked").each(function() { 
						dellist.push($(this).val());
					});
					
					$(".span9").block({
						showOverlay: true, 
						centerY: true, 
						css: { 
							width: '200px',  
							border: 'none', 
							padding: '20px', 
							backgroundColor: '#000', 
							'-webkit-border-radius': '10px', 
							'-moz-border-radius': '10px', 
							opacity: .6, 
							color: '#fff' 
						}, 
						message: '<font face="arial" size="4">Loading ...</font>'
						
					});
					
					$.ajax({
						type: "POST",
						url: "/DeleteLeaveEntitle",
						data: {dellist:dellist},
						success: function(response) {
							$(".span9").unblock();
							
							window.location.href = "admin-update-leave-entitle.jsp";
							
							return false;
						}
					});

					}

				});
			
			
            	$("#leaveEntitleList").dataTable({
                    "bServerSide": true,
                    "sAjaxSource": "/UpdateLeaveEntitle",
                    "bProcessing": true,
                    "bRetrieve": true,
                    "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
              		"sPaginationType": "bootstrap",
              		"oLanguage": {
              			"sLengthMenu": "_MENU_ records per page"
              		}
                });

        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
<!--     <div class="well sidebar-nav"> -->
    <jsp:include page="configuration-menu.jsp"></jsp:include>
<!--     </div> -->
    </div>
    <div class="span9" >
				<h5 id="Leave Entitle">LEAVE ENTITLE</h5>
				
				<hr></hr>
				<span>
				<a href="admin-add-leave-entitle.jsp" class=" btn btn-info">
				<i class="icon-black icon-pencil"></i> Add Leave Entitle</a>
				</span>
				<br></br>
				<form name="Update" method="post" action="UpdateLeaveEntitle">
					
					<table id="leaveEntitleList" cellpadding="0" cellspacing="0" 
					class="table table-striped table-bordered" >
					<thead id="headed">
						<tr>
							<th align="center" title="Edit">Edit</th>
							<th align="center" title="Delete">Delete</th>
							
							<th style="width:80px" "align="center" title="Region">Region</th>
							<th align="center" title="Entitled Annual">Entitled Annual</th> 
							<th align="center" title="Hospitalization">Hospitalization</th> 
							<th align="center" title="Maternity">Maternity</th>
							<th align="center" title="Birthday">Birthday</th>
							<th align="center" title="Wedding">Wedding</th>
							<th align="center" title="Compassionate">Compassionate</th>
							<th align="center" title="Compensation">Compensation</th>
						</tr>					
					</thead>
					<tbody></tbody>
				</table>
				<table>
					<tr>
						<td colspan="2">
							<a href="#" id="delete-leave-entitle" class="btn btn-primary" >Delete</a>
							&nbsp;&nbsp;&nbsp;
							<a href="#" onClick="javascript:clearForm()"  class="btn btn-danger">Reset</a>
						</td>
					</tr>
				</table>
				</form>
			</div>
			</div></div>
			<!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Delete Leave Entitle</h4>
			  </div>
			  <div class="modal-body">
			    <p>Please select at least one record to delete.</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
	</body>
</html>