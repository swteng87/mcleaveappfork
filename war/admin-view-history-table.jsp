<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="com.google.appengine.api.datastore.Entity"%>
<%@page import="java.util.List"%>
<%
float pageCount = (Float) request.getAttribute("pageCount");
List<Entity> result = (List<Entity>) request.getAttribute("results");
String footer = (String) request.getAttribute("footer");
%>

<table cellpadding="5" style="color:black;" border="0" id="view-history-table">
					<thead>
					<tr bgcolor="#C0C0C0">
						<th width="180" height="40" align="center" title="Time"><img src="images/clock.png" width="45px"/></th>
						<th width="250" height="40" align="center" title="Employee"><img src="images/emp.png" width="45px"/></th>
						<th width="100" height="40" align="center" title="Number of Days"><img src="images/day.png" width="45px"/></th>
						<th width="180" height="40" align="center" title="Start Date"><img src="images/start.png" width="45px"/></th>
						<th width="180" height="40" align="center" title="End Date"><img src="images/end.png" width="45px"/></th>
						<th width="150" height="40" align="center" title="Supervisor"><img src="images/supervisor.png" width="45px" alt="Supervisor" title="Supervisor"/></th>
						<th width="110" height="40" align="center" title="Leave Type"><img src="images/holiday.png" width="50px"/></th>
						<th width="150" height="40" align="center" title="Remark"><img src="images/comment.png" width="45px"/></th>
					</tr>
					</thead>
					<tbody>
						<% if(result == null){ %>
							<tr bgcolor="#FFF8C6">
								<td colspan="8"></td>
							</tr>
							<tr bgcolor="#F0F0F0">
								<td colspan="8">There are no records in the database for this region.</td>
							</tr>
						<%}else{ 
							for (Entity results : result) {	
						%>
							<tr>
								<td><font face="verdana" size="1"><%=results.getProperty("time") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("emailAdd") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("numOfDays") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("startDate") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("endDate") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("supervisor") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("leaveType") %></font></td>
								<td><font face="verdana" size="1"><%=results.getProperty("remark") %></font></td>
							</tr>
						<%
							} }%>
					</tbody>
					<tfoot>
						<tr><td colspan=10 class="id" style="text-align:right"><%=footer %></td></tr>
					</tfoot>
				</table>
				