<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<%
String emailAddress = (String) request.getAttribute("emailAddress"); if (emailAddress == null) { emailAddress = ""; }

String region = (String) request.getAttribute("region"); if (region == null) { region = ""; }

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
		<title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet"/>
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet"/>
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png"/>
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png"/>
    <style>
      undefined
    </style>
    <link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
	<script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
	<script src="assets/js/common.js"></script>
		<script type="text/javascript">
        $(document).ready(function () {
        	
        	function isSomethingChecked() {
        		var bool = false;
            		$('input:checkbox:checked').each(function(i) {
                         if(this.value != "" && this.value != "undefined"){
                         	bool = true;
                         }
                    });
        		return bool;
        	}
        	
//         	var running = 0;
			function Go(){
// 				var f = document.forms[0];	
				if (!isSomethingChecked()) {
				    /* alert( "Please select at least one region to submit." ); */
				    $('#myModal').modal('show')
				    return false;
				}else{
					return true;
// 					f.submit();
				}
				return true;
// 				running++;
			}
				
			
        	
			$("#update-supervisor").on("click",function(){
        		
            	if(Go()){
            		$(".span9").block({
    	    		 	showOverlay: true, 
    	    	        centerY: true, 
    	    	        css: { 
    	    	            width: '200px',  
    	    	            border: 'none', 
    	    	            padding: '15px', 
    	    	            backgroundColor: '#000', 
    	    	            '-webkit-border-radius': '10px', 
    	    	            '-moz-border-radius': '10px', 
    	    	            opacity: .6, 
    	    	            color: '#fff' 
    	    	        }, 
    	    			message: '<font face="arial" size="4">Loading ...</font>'
    	    			
    	    		});
            		var emailAddress = $("#emailAddress").val();
            		
					var region = new Array();
    				
            		$("input[name=region]:checked").each(function() { 
            			region.push($(this).val());
            	    });
            		
            		$("#msg").html("");
            		
    	    		$.ajax({
    	    			type: "POST",
    	    			url: "/UpdateSupervisorAction",
    	    			data: {emailAddress:emailAddress,region:region},
    	    			success: function(response) {
    	    				$(".span9").unblock();
    	                		
    	    				var code = $("#feedback", response).text();
                        	var message = $("#message", response).text();
    	    				
    	                		$("#msg").html(response);
    	                		
    	                		//window.location.href = "admin-update-emp.jsp";
    	                	
    	                	return false;
    	    			}
    	    		});
            	}
            	
    				
            	});
			
        });
		</script>
		
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <jsp:include page="account-menu.jsp"></jsp:include>
    <!-- </div> -->
    </div>
    <div class="span9">
				<h5 id="Supervisor">UPDATE SUPERVISOR</h5>
				<hr></hr>
				<div id="msg" class="error-msg"></div>
				<form name="Update" method="post" action="UpdateSupervisorAction">
					<input type=hidden name=cmd value="">
					<table cellpadding="10" border="0">
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:16px; color:black;">Email Address</td>
							<td width="100px">
								<div class="textbox">
									<input type="text" name="emailAddress" id="emailAddress" 
									value="<%=emailAddress%>" size="29" readonly="readonly"/>
								</div>
							</td>
						</tr>
						<tr>
							<td align="left" valign="top" style="padding-left:0px;padding-bottom:16px; color:black;">Region</td>
							<td>
								<div>
									<% 
										RegionsService rs = new RegionsService();
										SupervisorService ss = new SupervisorService();
										Supervisor supervisor = ss.findSupervisorByColumnName("emailAddress", emailAddress);
										for (int i =0; i<rs.getRegions().size(); i++) {
											Regions reg = rs.getRegions().get(i);
											
									%>
										<%if(supervisor.getRegion().contains(reg.getRegion())){ %>
											<label class="checkbox">&nbsp;<input type="checkbox" name="region" class="reg" 
											value=<%=reg.getRegion().replaceAll(" ", "-") %> checked />&nbsp;<%=reg.getRegion() %></label><br>
										<%}else{ %>
											<label class="checkbox">&nbsp;<input type="checkbox" name="region" class="reg" 
											value=<%=reg.getRegion().replaceAll(" ", "-") %> />&nbsp;<%=reg.getRegion() %></label><br>
										<%} %>
									
									<%} %>
								</div>
							</td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<a href="#" id="update-supervisor" class="btn btn-primary">Update</a>
							</td>
						</tr>
					</table>
				</form>
				
				</div></div></div>
				
				<!-- Modal -->
				<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
				    <h4 id="myModalLabel">Update Supervisor</h4>
				  </div>
				  <div class="modal-body">
				    <p>Please select at least one region to submit.</p>
				  </div>
				  <div class="modal-footer">			    
				    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
				  </div>
				</div>
	</body>
</html>