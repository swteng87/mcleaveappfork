<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%
String numOfDays = (String) request.getAttribute("numOfDays"); if (numOfDays == null) { numOfDays = ""; }

String remark = (String) request.getAttribute("remark"); if (remark == null) { remark = ""; }
	
String leaveType = (String) request.getAttribute("leaveType"); if (leaveType == null) { leaveType = ""; }
	
String startDate = (String) request.getAttribute("startDate"); if (startDate == null) { startDate = ""; }
	
String endDate = (String) request.getAttribute("endDate"); if (endDate == null) { endDate = ""; }

String approvalFrom = (String) request.getAttribute("approvalFrom"); if (approvalFrom == null) { approvalFrom = ""; }

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Master Concept</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
      undefined
    </style>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/datatable-bootstrap.js"></script>
    <script src="media/js/jquery.blockUI.js" type="text/javascript"></script>
		<script src="assets/js/common.js"></script>
		<link rel="stylesheet" href="media/themes/ui-lightness/jquery-ui-1.8.4.custom.css" />
		<script src="script/jquery-ui.js"></script>
		<script language="JavaScript">
// 			var running = 0;
			function Go() {
				var f = document.forms[0];	
				if (f.numOfDays.value == "" || f.startDate.value == "" 
						|| f.endDate.value == "" || f.remark.value == "" ||
						f.leaveType.value == "" || f.leaveType.value == "Default" ||
						f.approvalFrom.value =="" || f.approvalFrom.value=="Default" ) {
					/* alert("Please make sure all fields are filled."); */
					$('#myModal').modal('show')
					return false;
				}
				return true;
// 				f.submit();
// 				running++;
			}
			
		</script>
		<!--[if !IE 7]>
		<style type="text/css">
			#wrap {display:table;height:100%}
		</style>
		<![endif]-->
		<script type="text/javascript">
        $(document).ready(function () {
        	
        	$("#attachFile").hide();
        	$("#leaveType").change(function(){
        		if($(this).val() == "Sick Leave"){
        			$("#attachFile").show();
        		}
        		else{
        			$("#attachFile").hide();
        		}
        	});
        	
			var dates = new Array();
			var desc = new Array();

        	$.getJSON("ViewPublicHoliday", {},function(response){
        		$.each(response, function(key, value){
			    	dates.push(new Date(value.holiday));
			    	desc.push(value.desc);
			    });
        	});

        	function holiday(date) {

        	    for (var i = 0; i < dates.length; i++) {
        	        if (date - dates[i] == 0) {
        	            return [false, '', desc[i]];
        	        }
        	    }
        	    return [true];

        	}
        	
        	$("#startDate").datepicker({
        		beforeShowDay: holiday,
        		changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                onClose: function( selectedDate ) {
                    $( "#endDate" ).datepicker( "option", "minDate", selectedDate );
                  }
            });
        	
        	$("#endDate").datepicker({
        		beforeShowDay: holiday,
        		changeMonth: true,
                changeYear: true,
                dateFormat: "dd-mm-yy",
                onClose: function( selectedDate ) {
                    $( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
                  }
            });
        	
        	$("#add-leave-form").on("click",function(){
        		if(Go()){
        			$(".span9").block({
            		 	showOverlay: true, 
            	        centerY: true, 
            	        css: { 
            	            width: '200px',  
            	            border: 'none', 
            	            padding: '15px', 
            	            backgroundColor: '#000', 
            	            '-webkit-border-radius': '10px', 
            	            '-moz-border-radius': '10px', 
            	            opacity: .6, 
            	            color: '#fff' 
            	        }, 
            			message: '<font face="arial" size="4">Loading ...</font>'
            			
            		});
            		
            		var data = $("form").serialize();
            		
            		$.ajax({
            			type: "POST",
            			url: "/ApplyLeave",
            			data: data,
            			success: function(response) {
            				$(".span9").unblock();
                        	
            				$("#msg").html(response);
                        	
                        	return false;
            			}
            		});
        		}
        	});
        	
        });
        </script>
         <!-- The standard Google Loader script; use your own key. -->
    <script src="http://www.google.com/jsapi?key=AIzaSyBRTYRqK4LHZY3DtmBXloifBRDcrdvcvKY"></script>
    <script type="text/javascript">

 // Use the Google Loader script to load the google.picker script.
// 	google.setOnLoadCallback(createPicker);
//     google.load('picker', '1');	
    
    function attachment() {
        google.load('picker', '1', {"callback" : createPicker});
    }
    

    // Create and render a Picker object for searching images.
    function createPicker() {
    	
      var view = new google.picker.View(google.picker.ViewId.DOCS);
//      view.setMimeTypes("image/png,image/jpeg,image/jpg,text/plain");    
      var picker = new google.picker.PickerBuilder()
//           .enableFeature(google.picker.Feature.NAV_HIDDEN)
          .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
          .setAppId("29347096620.apps.googleusercontent.com")
          //.setOAuthToken(AUTH_TOKEN) //Optional: The auth token used in the current Drive API session.
//           .addView(view)
// 		  .addView(google.picker.ViewId.IMAGE_SEARCH)
		  .addView(google.picker.ViewId.DOCUMENTS)
		  .addView(google.picker.ViewId.SPREADSHEETS)
		  .addView(google.picker.ViewId.DOCS_IMAGES)
          .addView(new google.picker.DocsUploadView())
          .setCallback(pickerCallback)
          .build();
       picker.setVisible(true);
    }

    // A simple callback implementation.
//     function pickerCallback(data) {
//       if (data.action == google.picker.Action.PICKED) {
//         var fileId = data.docs[0].id;
//         alert('The user selected: ' + fileId);
		 
//       }
//     }
    
    function pickerCallback(data) {
      var url = new Array();
      
      if (data[google.picker.Response.ACTION] == google.picker.Action.PICKED) {
    	  
    			  for (var key in data) {
    	    		  if (data.hasOwnProperty(key)) {
    	    			  var doc = data[google.picker.Response.DOCUMENTS][0];
    	    			  url.push(doc[google.picker.Document.URL]);
//     	    	          url.push(doc[google.picker.Document.NAME]);
    			  
    		  }
    		}
       
        
      }
      
      
      
      for (var i in url) {
    	  $("#attachUrl").html('Attachment File Url : <a href="'+url[i]+'">'+url[i]+'</a>');
    	  var f = document.forms[0];	
    	  f.attachmentUrl.value = url[i];
// 			$("#attachUrl").html('Attachment File Name : '+url[i]);
      }
      
    }
    
    
    </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-emp-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav"> -->
    <jsp:include page="leave-emp-menu.jsp"></jsp:include>
    <!-- </div> -->
    </div>
    <div class="span9" >
				<h5 id="Leave Application Form">LEAVE APPLICATION FORM</h5>
				<hr></hr>
				<div class="error-msg" id="msg">
				</div>
				<form name="Apply" method="post" action="ApplyLeave">
					<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
					<jsp:setProperty name="jspbeans" property="*"/>
					<input type="hidden" name="attachmentUrl">
					<table cellpadding="5" style="color:black;" border="0">
						<tr>
							<td align="left" style="padding-left:0px;">Number of Days <span class="error-msg">*</span></td>
							<td width="230px">
								<div class="textbox">
									<input type="text" name="numOfDays" value="<%=numOfDays%>" maxlength="3"/>
								</div>
							</td>
							<td>* (LIST BUSINESS DAYS ONLY) Please enter 0.5 for half day application</td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;padding-bottom:15px;" width="127px">Start Date of Leave <span class="error-msg">*</span></td>
							<td>
								<div class="textbox">
									<input type="text" id="startDate" name="startDate" value="<%=startDate%>" readonly/>
								</div>
							</td>
							<td></td>
							</tr>
						<tr>
						<tr>
							<td align="left" style="padding-left:0px;">End Date of Leave <span class="error-msg">*</span></td>
							<td>
								<div class="textbox">
									<input type="text" id="endDate" name="endDate" value="<%=endDate%>" readonly/>
								</div>
							</td>
							<td>	
							</td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;">Leave Type <span class="error-msg">*</span></td>
							<td>
								<div class="dropdown">
									<select name="leaveType" id="leaveType">
										<%//=jspbeans.listLeaveType()%>
										<option value="Default">-- Please select leave type --</option>
										<option value="Annual Leave">Annual Leave</option>
										<option value="Sick Leave">Sick Leave</option>
										<option value="Compensation Leave">Compensation Leave</option>
										<option value="No Pay Leave">No Pay Leave</option>
										<option value="Maternity Leave">Maternity Leave</option>
										<option value="Birthday Leave">Birthday Leave</option>
										<option value="Wedding Leave">Wedding Leave</option>
										<option value="Compassionate Leave">Compassionate Leave</option>
										<option value="Others">Others</option>
									</select>
									<script>
										this.document.forms[0].leaveType.value="<%=leaveType%>"; 
									</script>
								</div>
							</td>
							<td>* Sick leave please with attachment doctor letter / receipt.
							
							<br>* No pay leave included weekend
							</td>
						</tr>
						<tr id="attachFile">
							<td></td>
							<td>
								<div><a href="#" name="attachment" id="attachment" class="btn btn-info pull-left" onclick="attachment()">Attachment</a></div>
								<br><br>
								<div style="width:230px; word-wrap:break-word;" id="attachUrl"></div>
							</td>
							<td>* Please share the file to judy.ng@hkmci.com , HR need to check the document
							<br>* Please share the file to amy.ng@hkmci.com as well for region MY / SG.</td>
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;" valign="top">Remark <span class="error-msg">*</span></td>
							<td>
							<textarea name="remark" id="remark" rows="8" cols="27"><%=remark%></textarea>
							</td>
							<td valign="top">* Please state the details in the remark column.</td>
						
						</tr>
						<tr>
							<td align="left" style="padding-left:0px;">Approval From <span class="error-msg">*</span></td>
							<td><div class="textbox">
									<input type="text" name="approvalFrom" value="<%=approvalFrom%>" readonly/>
							</div></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td colspan="2">
								<a href="#" id="add-leave-form" class="btn btn-primary">Submit</a>
							</td>
							
						</tr>
					</table>
				</form>
				
				
			</div>
			</div></div>
			
			<!-- Modal -->
			<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			  <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
			    <h4 id="myModalLabel">Leave Application Form</h4>
			  </div>
			  <div class="modal-body">
			    <p>Please make sure all fields are filled.</p>
			  </div>
			  <div class="modal-footer">			    
			    <button class="btn btn-primary" data-dismiss="modal" aria-hidden="true">Ok</button>
			  </div>
			</div>
	</body>
</html>