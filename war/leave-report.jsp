<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>
<%@page import="com.google.appengine.util.*"%>
<%@page import="java.util.*"%>
<%@ page import="java.util.Calendar" %>
<%
String isAdmin = (String)request.getAttribute("isAdmin");
String result = (String) request.getAttribute("result");
String cri_region = (String) request.getAttribute("cri_region"); if (cri_region == null) { cri_region = ""; }

Calendar cal = Calendar.getInstance();
int cal_year = cal.get(Calendar.YEAR);
String cri_year = (String) request.getAttribute("cri_year"); if(cri_year == null) {cri_year = String.valueOf(cal_year);}


Double janAL = (Double)request.getAttribute("janAL");
Double janSL = (Double)request.getAttribute("janSL");
Double janCL = (Double)request.getAttribute("janCL");
Double janCLE = (Double)request.getAttribute("janCLE");

Double febAL = (Double)request.getAttribute("febAL");
Double febSL = (Double)request.getAttribute("febSL");
Double febCL = (Double)request.getAttribute("febCL");
Double febCLE = (Double)request.getAttribute("febCLE");

Double marAL = (Double)request.getAttribute("marAL");
Double marSL = (Double)request.getAttribute("marSL");
Double marCL = (Double)request.getAttribute("marCL");
Double marCLE = (Double)request.getAttribute("marCLE");

Double aprAL = (Double)request.getAttribute("aprAL");
Double aprSL = (Double)request.getAttribute("aprSL");
Double aprCL = (Double)request.getAttribute("aprCL");
Double aprCLE = (Double)request.getAttribute("aprCLE");

Double mayAL = (Double)request.getAttribute("mayAL");
Double maySL = (Double)request.getAttribute("maySL");
Double mayCL = (Double)request.getAttribute("mayCL");
Double mayCLE = (Double)request.getAttribute("mayCLE");

Double junAL = (Double)request.getAttribute("junAL");
Double junSL = (Double)request.getAttribute("junSL");
Double junCL = (Double)request.getAttribute("junCL");
Double junCLE = (Double)request.getAttribute("junCLE");

Double julAL = (Double)request.getAttribute("julAL");
Double julSL = (Double)request.getAttribute("julSL");
Double julCL = (Double)request.getAttribute("julCL");
Double julCLE = (Double)request.getAttribute("julCLE");

Double augAL = (Double)request.getAttribute("augAL");
Double augSL = (Double)request.getAttribute("augSL");
Double augCL = (Double)request.getAttribute("augCL");
Double augCLE = (Double)request.getAttribute("augCLE");

Double sepAL = (Double)request.getAttribute("sepAL");
Double sepSL = (Double)request.getAttribute("sepSL");
Double sepCL = (Double)request.getAttribute("sepCL");
Double sepCLE = (Double)request.getAttribute("sepCLE");

Double octAL = (Double)request.getAttribute("octAL");
Double octSL = (Double)request.getAttribute("octSL");
Double octCL = (Double)request.getAttribute("octCL");
Double octCLE = (Double)request.getAttribute("octCLE");

Double novAL = (Double)request.getAttribute("novAL");
Double novSL = (Double)request.getAttribute("novSL");
Double novCL = (Double)request.getAttribute("novCL");
Double novCLE = (Double)request.getAttribute("novCLE");

Double decAL = (Double)request.getAttribute("decAL");
Double decSL = (Double)request.getAttribute("decSL");
Double decCL = (Double)request.getAttribute("decCL");
Double decCLE = (Double)request.getAttribute("decCLE");

%>
<html>
  <head>
  <title>Master Concept Admin</title>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body { padding-top: 45px; /* 60px to make the container go all the way
      to the bottom of the topbar */
      padding-bottom: 40px;
       }
       .sidebar-nav {
        padding: 0px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="assets/css/docs.css" rel="stylesheet"/>
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
      </script>
    <![endif]-->
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <style>
      .google-visualization-orgchart-node {
    		border: 0px solid rgb(181, 217, 234) !important;
		}
    </style>
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap.js"></script>
    <script src="assets/js/common.js"></script>
    <script type='text/javascript' src='https://www.google.com/jsapi'></script>
    <script type='text/javascript'>
      google.load('visualization', '1', {packages:['corechart']});
      function drawVisualization() {
          // Create and populate the data table.
          var data = google.visualization.arrayToDataTable([
            ['Month', 'Annual Leave', 'Compensation Leave', 'Compensation Leave Entitle', 'Sick Leave'],
            ['Jan',  <%=janAL%>,   <%=janSL%>,   <%=janCL%>,  <%=janCLE%>],
            ['Feb',  <%=febAL%>,   <%=febSL%>,   <%=febCL%>,  <%=febCLE%>],
            ['Mar',  <%=marAL%>,   <%=marSL%>,   <%=marCL%>,  <%=marCLE%>],
            ['Apr', <%=aprAL%>,   <%=aprSL%>,   <%=aprCL%>,  <%=aprCLE%>],
            ['May', <%=mayAL%>,   <%=maySL%>,   <%=mayCL%>,  <%=mayCLE%>],
            ['Jun',   <%=junAL%>,   <%=junSL%>,   <%=junCL%>,  <%=junCLE%>],
            ['Jul',  <%=julAL%>,   <%=julSL%>,   <%=julCL%>,  <%=julCLE%>],
            ['Aug', <%=augAL%>,   <%=augSL%>,   <%=augCL%>,  <%=augCLE%>],
            ['Sep',  <%=sepAL%>,   <%=sepSL%>,   <%=sepCL%>,  <%=sepCLE%>],
            ['Oct', <%=octAL%>,   <%=octSL%>,   <%=octCL%>,  <%=octCLE%>],
            ['Nov',  <%=novAL%>,   <%=novSL%>,   <%=novCL%>,  <%=novCLE%>],
            ['Dec',  <%=decAL%>,   <%=decSL%>,   <%=decCL%>,  <%=decCLE%>]

          ]);
        
          // Create and draw the visualization.
          new google.visualization.ColumnChart(document.getElementById('chart_div')).
              draw(data,
                   {title: <%=cri_year%> + " Monthly Leave Report",
                    width:800, height:400,
                    hAxis: {title: "Month"}}
              );
        }
        

        google.setOnLoadCallback(drawVisualization);
        
        function Go() {
      	  var f = document.forms[0];
      	  f.submit();
        }
      </script>
    <script>
    $(document).ready(function () {
        $("#region").on("change", function (){
			Go();
        });
        $("#cri_year").on("change", function (){
			Go();
        });
    });
    </script>
  </head>

  <body>
  <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
      <div class="container">
      <jsp:include page="top-menu.jsp"></jsp:include>
      </div>
      </div>
    </div>
	<div class="container-fluid">
    <div class="row-fluid">
    <div class="span3" >
    <!-- <div class="well sidebar-nav">
    <ul class="nav nav-list"> -->
    <jsp:include page="report-menu.jsp"></jsp:include>
	<!-- </ul>
    </div> -->
    </div>
    <div class="span9" >
				<h5 id="Region Leave Report">Report</h5>
				<hr></hr>
				<form name="RegionLeaveReport" action="RegionLeaveReport">
				<jsp:useBean id="jspbeans" scope="page" class="com.google.appengine.mct.Misc"></jsp:useBean>
				<jsp:setProperty name="jspbeans" property="*"/>
				<div>
					<i>To view, please select the region to narrow down your search.</i>
				</div>
				<div>
							<span>Region
								
								<% 
									RegionsService rs = new RegionsService();
									if(rs != null){
									%>
									<select name="cri_region" id="region">
									<option value=""> Please select a region </option>
									<% 	
										for (int i =0; i<rs.getRegions().size(); i++) {
											Regions reg = rs.getRegions().get(i);
									%>
									<option <%if(reg.getRegion().equals(cri_region)){ %> selected<%} %> 
									value="<%=reg.getRegion()%>"><%=reg.getRegion() %></option>
											
										<%} %>
									</select>
									<%}else{ %>
										No region found
									<%} %>
								
							</span>
							<span>Year
									<select name="cri_year" id="cri_year">
									<%for(int i= cal_year; i>=1930; i--){ %>
									<option <%if(i==Integer.parseInt(cri_year)){%> selected <%}%> value="<%=i%>"><%=i %></option>
									<%} %>
									</select>
							</span>
							
				</div>
				</form>
    <div id='chart_div' style="border: 0 none; width: 800px; height: 400px;"></div>
    </div></div></div>
  </body>
</html>