
			    <p><a href="#login">How to login ?</a></p>
			     
			    <p><a href="#applyleave">How to apply leave ?</a></p>
			    <p><a href="#applycomp">How to apply compensation leave ?</a></p>
			    <p><a href="#cancel">How to cancel and amendment leave ?</a></p>
			      <p><a href="#history">How to view leave history ?</a></p>
			       <p><a href="#leavedetails">How to view leave details ?</a></p>
			    
			    
			    <p><a href="#approve">How to approve leave requests ?</a></p>
			    <p><a href="#reject">How to reject leave requests ?</a></p>
			    <p><a href="#pendingqueue">How to view pending queue ?</a></p>
			   
			    <section id="login">
			    <p><b><font size="3">How to login</font></b></p>
			    <ol>
			    	<li>log in with your google account, go to top navigation menu bar, click more and find the MCLeave link.</li>
			    	<li>login into http://mcleaveapp.appspot.com/, please make sure you are logging into google account.</li>
			    </ol>
			    </section>
			    <section id="applyleave">
			    <p><b><font size="3">How to apply leave ?</font></b></p>
			    <ol>
			    	<li>select click leaves application form.</li>
			    	<li>all field is mandatory.</li>
			    	<li>enter number of days you want to apply.</li>
			    	<li>select start date and end date.</li>
			    	<li>start date and end date must be equal to the number of day you apply.</li>
			    	<li>sick leave must attach to doctor letter / receipt.</li>
			    	<li>sick leave must share the attached with respective regional HR.</li>
			    	<li>sick leave must not exceed total sick in respective region.</li>
			    	<li>annual leave apply must not exceed total leave balance.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    
			   <section id="applycomp">
			    <p><b><font size="3">How to apply compensation leave ?</font></b></p>
			    <ol>
			    	<li>select compensation entitlement leaves form.</li>
			    	<li>all field is mandatory.</li>
			    	<li>enter number of days you want to apply.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    <section id="cancel">
			     <p><b><font size="3">How to cancel and amendment leave ?</font></b></p>
			    <ol>
			    	<li>select amend /cancel leave.</li>
			    	<li>select the day you want to amend.</li>
			    	<li>if you select amend leave, new start date and new end date are mandatory.</li>
			    	<li>if you select cancel leave, ignore the new start date and the new end date.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    
			    <section id="history">
			     <p><b><font size="3">How to view leave history ?</font></b></p>
			    <ol>
			    	<li>select view my leave history.</li>
			    </ol>
			    </section>
			    
			    <section id="leavedetails">
			     <p><b><font size="3">How to view my leave details ?</font></b></p>
			    <ol>
			    	<li>select view my leave details.</li>
			    </ol>
			    </section>
			    
			    <section id="approve">
			     <p><b><font size="3">How to approve leave requests ?</font></b></p>
			    <ol>
			   		<li>select view approved leave requests.</li> 
			    	<li>only the approved leave request will show on the list.</li>
			    </ol>
			    </section>
			    
			    <section id="reject">
			     <p><b><font size="3">How to reject leave requests ?</font></b></p>
			    <ol>
			   		<li>select view reject leave requests.</li> 
			    	<li>only the reject leave request will show on the list.</li>
			    </ol>
			    </section>
			    
			    <section id="approvereject">
			     <p><b><font size="3">How to approve and reject leave ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select master concept leave, click approve/ reject leave requests.</li>
			    	<li>select the record you want to approve or reject.</li>
			    	<li>you can select multiple record approve or reject in the same time.</li>
			    	<li>only the employee under your supervision is visible in you pending approval list.</li>
			    	<li>only the employee under your supervision is you able to approve or rejected leave request.</li>
			    </ol>
			    </section>
			    
			    
			    
			    