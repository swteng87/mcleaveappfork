<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="ISO-8859-1"%>

<%@page import="com.google.appengine.mct.*"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta content="utf-8" http-equiv="encoding"/>
		<title>Master Concept</title>
		<link rel="stylesheet" href="css/new-style.css" type="text/css" media="screen" />
		<link rel="shortcut icon" href="images/favicon.ico" sizes="64x64" type="image/png"/>
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    	<meta name="description" content=""/>
    	<meta name="author" content=""/>
    	<!-- Le styles -->
    	<link href="assets/css/bootstrap.css" rel="stylesheet">
    	<style>
	    	body { padding-top: 45px; /* 60px to make the container go all the way
	     	to the bottom of the topbar */
	      	padding-bottom: 40px;
	       	}
       		.sidebar-nav {
        	padding: 0px 0;
      		}
      		@media (max-width: 980px) {
        		/* Enable use of floated navbar text */
        		.navbar-text.pull-right {
          		float: none;
          		padding-left: 5px;
          		padding-right: 5px;
        		}
      		}
    	</style>
    	<link href="assets/css/docs.css" rel="stylesheet"/>
	    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
	    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	    <!--[if lt IE 9]>
	      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js">
	      </script>
	    <![endif]-->
	    <!-- Le fav and touch icons -->
	    <link rel="shortcut icon" href="assets/ico/favicon.ico">
	    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
	    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
	    <!--[if !IE]><!-->
		<style>
		
		/* 
		Max width before this PARTICULAR table gets nasty
		This query will take effect for any screen smaller than 760px
		and also iPads specifically.
		*/
		@media 
		only screen and (max-width: 760px),
		(min-device-width: 768px) and (max-device-width: 1024px)  {
	
			/* Force table to not be like tables anymore */
			#historyList table,
		 	#historyList thead, 
		 	#historyList tbody, 
		 	#historyList th,
		  	#historyList td, 
		  	#historyList tr { 
			display: block; 
			}
		
			/* Hide table headers (but not display: none;, for accessibility) */
			#historyList thead tr { 
				position: absolute;
				top: -9999px;
				left: -9999px;
			}
		
/* 		#employeeList tr { border: 1px solid #ccc; } */
		
			#historyList td { 
			/* Behave  like a "row" */
/*  		border: none;  */
/* 			border-bottom: 1px solid #eee;  */
				position: relative;
				padding-left: 50%; 
				white-space: normal;
				text-align:left;
			}
		
			#historyList td:before { 
			/* Now like a table header */
				position: absolute;
				/* Top/left values mimic padding */
				top: 6px;
				left: 6px;
				width: 45%; 
				padding-right: 10px; 
				white-space: nowrap;
				text-align:left;
		}
		
		/*
		Label the data
		*/
 		td:nth-of-type(1):before { content: "Time"; } 
 		td:nth-of-type(2):before { content: "Employee"; } 
 		td:nth-of-type(3):before { content: "Number of Days"; } 
 		td:nth-of-type(4):before { content: "Start Date"; } 
 		td:nth-of-type(5):before { content: "End Date"; }
 		td:nth-of-type(6):before { content: "Supervisor"; }
 		td:nth-of-type(7):before { content: "Leave Type"; }
 		td:nth-of-type(8):before { content: "Remark"; } 
/* 		#employeeList td:before { content: attr(data-title); } */
		}
	
		/* Smartphones (portrait and landscape) ----------- */
		@media only screen
		and (min-device-width : 320px)
		and (max-device-width : 480px) {
			body { 
				padding: 0; 
				margin: 0; 
				width: 320px; }
			}
	
		/* iPads (portrait and landscape) ----------- */
		@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
			body { 
				width: 495px; 
			}
		}
	
		</style>
		<!--<![endif]-->
    	<link rel="stylesheet" type="text/css" href="assets/css/datatable-bootstrap.css"/>
    	<script src="media/js/jquery.js" type="text/javascript"></script>
    	<script src="media/js/jquery.dataTables.min.js" type="text/javascript"></script>
		<script src="assets/js/bootstrap.js"></script>
    	<script src="assets/js/datatable-bootstrap.js"></script>
     	<script src="assets/js/common.js"></script>
        <script type="text/javascript">
        $(document).ready(function () {
     
            	$("#historyList").dataTable({
                    "bServerSide": true,
                    "sAjaxSource": "/ViewHistory",
                    "bProcessing": true,
                    "bRetrieve": true,
                    "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>",
              		"sPaginationType": "bootstrap",
              		"oLanguage": {
              			"sLengthMenu": "_MENU_ records per page"
              		}
                });
            	
        });
        </script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
      		<div class="navbar-inner">
      			<div class="container">
      				<jsp:include page="top-emp-menu.jsp"></jsp:include>
      			</div>
      		</div>
    	</div>
		<div class="container-fluid">
    		<div class="row-fluid">
    			<div class="span3" >
    				<!-- <div class="well sidebar-nav"> -->
    						<jsp:include page="leave-emp-menu.jsp"></jsp:include>
    				<!-- </div> -->
    			</div>
		<div class="span9">
				<h5 id="Leave History">LEAVE HISTORY</h5>
				<hr></hr>
					<!-- Add Approve Requests button and Rejected Requests button -->
					<table style="width: 100%;">					
						<tr>
							<td style="padding-bottom:18px; float:right;" >
								<a href="mct-view-approved.jsp" class="btn btn-info" >Approved Requests</a>
								&nbsp;&nbsp;&nbsp;&nbsp;
								<a href="mct-view-rejected.jsp" class="btn btn-info" >Rejected Requests</a>
							</td>
						</tr>
					</table>
					
					<table  id="historyList" cellpadding="0" cellspacing="0"
					class="table table-striped table-bordered" >
					<thead id="headed">
						<tr>
							<th style="width:85px; word-wrap:break-word; align:center;" title="Time">Time</th>
							<th align="center" title="Employee">Employee</th>
							<th align="center" title="Number of Days">Number of Days</th>
							<th align="center" title="Start Date">Start Date</th>
							<th align="center" title="End Date">End Date</th>
							<th align="center" title="Supervisor">Supervisor</th>
							<th align="center" title="Leave Type">Leave Type</th>
							<th align="center" title="Change Type">Change Type</th>
							<th align="center" title="Remark">Remark</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
					</div>
				</div>
			</div>
	</body>
</html>