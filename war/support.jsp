
			    <p><a href="#login">How to login ?</a></p>
			     <p><a href="#addempleavedetails">How to add employee leave details ?</a></p>
			   <p><a href="#deleteempleavedetails">How to delete employee leave details ?</a></p>
			   <p><a href="#updateempleavedetails">How to update employee leave details ?</a></p>
			     <p><a href="#viewempleavedetails">How to view employee leave details ?</a></p>
			    <p><a href="#applyleave">How to apply leave ?</a></p>
			    <p><a href="#applycomp">How to apply compensation leave ?</a></p>
			    <p><a href="#cancel">How to cancel and amendment leave ?</a></p>
			    <p><a href="#approve">How to approve and reject leave ?</a></p>
			   
			    <p><a href="#addleaverequest">How to add leave request ?</a></p>
			    <p><a href="#viewleaverequest">How to view approved leave request ?</a></p>
			    <p><a href="#deleteleaverequest">How to delete approved leave request ?</a></p>
			    <p><a href="#viewreject">How to view rejected leave requests ?</a></p>
			   <p><a href="#deletereject">How to delete rejected leave requests ?</a></p>
			   
			      <p><a href="#viewpending">How to view pending leave queue ?</a></p>
			       <p><a href="#deletepending">How to delete pending leave queue ?</a></p>
			       
			        <p><a href="#setting">How to configure setting ?</a></p>
			    <p><a href="#notify">How to receive notification email ?</a></p>
			    
			      <p><a href="#addadmin">How to add administrator ?</a></p>
			      <p><a href="#viewadmin">How to view administrator ?</a></p>
			      <p><a href="#deleteadmin">How to delete administrator ?</a></p>
			      
			       <p><a href="#addemployee">How to add employee ?</a></p>
			         <p><a href="#viewemployee">How to view employee ?</a></p>
			           <p><a href="#deleteemployee">How to delete employee ?</a></p>
			             <p><a href="#updateemployee">How to update employee ?</a></p>
			             
			     <p><a href="#addsupervisor">How to add supervisor ?</a></p>        
			      <p><a href="#viewsupervisor">How to view supervisor ?</a></p>         
			        <p><a href="#updatesupervisor">How to update supervisor ?</a></p> 
			         <p><a href="#deletesupervisor">How to delete supervisor ?</a></p> 
			         
			          <p><a href="#addregion">How to add region ?</a></p> 
			           <p><a href="#viewregion">How to view region ?</a></p> 
			           <p><a href="#deleteregion">How to delete region ?</a></p> 
			           
			           <p><a href="#addholiday">How to add holiday ?</a></p> 
			           <p><a href="#viewholiday">How to view holiday ?</a></p> 
			           <p><a href="#deleteholiday">How to delete holiday ?</a></p> 
			           
			           <p><a href="#viewhistory">How to view history ?</a></p> 
			           <p><a href="#deletehistory">How to delete history ?</a></p> 
			   
			    <section id="login">
			    <p><b><font size="3">How to login</font></b></p>
			    <ol>
			    	<li>log in with your google account, go to top navigation menu bar, click more and find the MCLeave link.</li>
			    	<li>login into http://mcleaveapp.appspot.com/, please make sure you are logging into google account.</li>
			    </ol>
			    </section>
			    <section id="applyleave">
			    <p><b><font size="3">How to apply leave ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select master concept leave, click leave application form.</li>
			    	<li>all field is mandatory.</li>
			    	<li>enter number of days you want to apply.</li>
			    	<li>select start date and end date.</li>
			    	<li>start date and end date must be equal to number of days you apply.</li>
			    	<li>sick leave must attach to doctor letter / receipt.</li>
			    	<li>sick leave must share the attached with respective regional HR.</li>
			    	<li>sick leave must not exceed total sick in respective region.</li>
			    	<li>annual leave apply must not exceed total leave balance.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    
			   <section id="applycomp">
			    <p><b><font size="3">How to apply compensation leave ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select master concept leave, click compensation entitlement leave form.</li>
			    	<li>all field is mandatory.</li>
			    	<li>enter number of day you want to apply.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    <section id="cancel">
			     <p><b><font size="3">How to cancel and amendment leave ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select master concept leave, click amend /cancel leave.</li>
			    	<li>select the day you want to amend.</li>
			    	<li>if you select amend leave, the new start date and the new end date are mandatory.</li>
			    	<li>if you select cancel leave, ignore the new start date and the new end date.</li>
			    	<li>please refer to the leave policy for more detail.</li>
			    </ol>
			    </section>
			    <section id="approve">
			     <p><b><font size="3">How to approve and reject leave ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select master concept leave, click approve/ reject leave request.</li>
			    	<li>select the record you want to approve.</li>
			    	<li>you can select multiple record approve or reject in the same time.</li>
			    	<li>only the employee under your supervision is visible in you pending approval list.</li>
			    	<li>administrator able to view all employees pending approval record.</li>
			    	<li>administrator able to approve or reject all employees pending approval request.</li>
			    </ol>
			    </section>
			    <section id="setting">
			     <p><b><font size="3">How to configure setting ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select configuration, click system settings.</li>
			    	<li>App domain - application domain, used for SSO validation and authorization, authentication to all user.</li>
			    	<li>Calendar Service Account - create and update calendar service.</li>
			    	<li>Calendar Service Account Password</li>
			    	<li>System Admin Email Address - system failure or system error will send email notification to system administrator</li>
			    	<li>App Admin Account - Application domain account</li>
			    	<li>App Admin Account Password</li>
			    	<li>Spreadsheet Service Account - create and update spreadsheet service.</li>
			    	<li>Spreadsheet Service Account Password</li>
			    	<li>Email Sender Account - email account to create send email service.</li>
			    </ol>
			    </section>
			    <section id="notify">
			    <p><b><font size="3">How to receive notification email ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select configuration, click add email notification.</li>
			    	<li>enter email address and select region wish to receive the email notify.</li>
			    	<li>to update the email notification, select the region and choose a record to delete or update.</li>
			    </ol>
			    </section>
			    
			    <section id="addleaverequest">
			    <p><b><font size="3">How to add leave request ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, click leave requests.</li>
			    	<li>select add leave requests.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			     <section id="viewleaverequest">
			    <p><b><font size="3">How to add view approved leave request ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, click leave requests.</li>
			    	<li>select view leave requests.</li>
			    </ol>
			    </section>
			    
			     <section id="deleteleaverequest">
			    <p><b><font size="3">How to delete approved leave request ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, click leave requests.</li>
			    	<li>select delete approved requests.</li>
			    </ol>
			    </section>
			    
			    <section id="viewreject">
			    <p><b><font size="3">How to view rejected leave requests ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, click leave requests.</li>
			    	<li>select view reject requests.</li>
			    </ol>
			    </section>	
			    
			    <section id="deletereject">
			    <p><b><font size="3">How to delete rejected leave requests ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, click leave requests.</li>
			    	<li>select delete reject requests.</li>
			    </ol>
			    </section>	
			    
			    <section id="addempleavedetails">
			    <p><b><font size="3">How to add employee leave details ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select employee leave details.</li>
			    	<li>select add employee leave details</li>
			    	<li>all field are mandatory.</li>
			    	<li>employee leaves details must be filled in first, in order to apply leave.</li>
			    </ol>
			    </section>	
			    
			     <section id="deleteempleavedetails">
			    <p><b><font size="3">How to delete employee leave details ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select employee leave details.</li>
			    	<li>select delete employee leave details</li>
			    </ol>
			    </section>	
			    
			     <section id="updateempleavedetails">
			    <p><b><font size="3">How to update employee leave details ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select employee leave details.</li>
			    	<li>select update employee leave details</li>
			    </ol>
			    </section>
			    
			     <section id="viewempleavedetails">
			    <p><b><font size="3">How to view employee leave details ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select employee leave details.</li>
			    	<li>select view employee leave details</li>
			    </ol>
			    </section>
			    
			      <section id="viewpending">
			    <p><b><font size="3">How to view pending leave queue ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select pending leave queue.</li>
			    	<li>select view pending leave queue.</li>
			    </ol>
			    </section>
			    
			      <section id="deletepending">
			    <p><b><font size="3">How to delete pending leave queue ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select pending leave queue.</li>
			    	<li>select delete pending leave queue.</li>
			    </ol>
			    </section>
			    
			     <section id="adadmin">
			    <p><b><font size="3">How to add administrator ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select add admin.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			    <section id="viewadmin">
			    <p><b><font size="3">How to view administrator ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view admin.</li>
			    </ol>
			    </section>
			    
			    <section id="deleteadmin">
			    <p><b><font size="3">How to view administrator ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete admin.</li>
			    </ol>
			    </section>
			    
			    <section id="addemployee">
			    <p><b><font size="3">How to add employee ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select add employee.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			     <section id="viewemployee">
			    <p><b><font size="3">How to view employee ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view employee.</li>
			    </ol>
			    </section>
			    
			     <section id="deleteemployee">
			    <p><b><font size="3">How to delete employee ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete employee.</li>
			    </ol>
			    </section>
			    
			    <section id="updateemployee">
			    <p><b><font size="3">How to update employee ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select update employee.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			    <section id="addsupervisor">
			    <p><b><font size="3">How to add supervisor ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select add supervisor.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			    <section id="viewsupervisor">
			    <p><b><font size="3">How to view supervisor ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view supervisor.</li>
			    </ol>
			    </section>
			    
			    <section id="updatesupervisor">
			    <p><b><font size="3">How to update supervisor ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select update supervisor.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			      <section id="deletesupervisor">
			    <p><b><font size="3">How to update supervisor ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete supervisor.</li>
			    </ol>
			    </section>
			    
			    <section id="addregion">
			    <p><b><font size="3">How to add region ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select add region.</li>
			    	<li>all field are mandatory.</li>
			    </ol>
			    </section>
			    
			    <section id="viewregion">
			    <p><b><font size="3">How to view region ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view region.</li>
			    </ol>
			    </section>
			    
			    <section id="deleteregion">
			    <p><b><font size="3">How to delete region ?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete region.</li>
			    </ol>
			    </section>
			    
			    <section id="addholiday">
			    <p><b><font size="3">How to add region holiday?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select add holiday.</li>
			    	<li>all field are mandatory.</li>
			    	<li>leave apply will base on region holiday.</li>
			    </ol>
			    </section>
			    
			    <section id="viewholiday">
			    <p><b><font size="3">How to view region holiday?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view holiday.</li>
			    </ol>
			    </section>
			    
			     <section id="deleteholiday">
			    <p><b><font size="3">How to delete region holiday?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete holiday.</li>
			    </ol>
			    </section>
			    
			     <section id="viewhistory">
			    <p><b><font size="3">How to view history?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select view history.</li>
			    </ol>
			    </section>
			    
			     <section id="deletehistory">
			    <p><b><font size="3">How to delete history?</font></b></p>
			    <ol>
			    	<li>go to top navigation menu bar select leave, select account.</li>
			    	<li>select delete history.</li>
			    </ol>
			    </section>
			    
			    
			    