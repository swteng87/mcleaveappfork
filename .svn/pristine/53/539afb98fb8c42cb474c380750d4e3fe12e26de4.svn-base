package com.google.appengine.mct;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.util.ConstantUtils;

@SuppressWarnings("serial")
public class AddLeaveEntitle extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(AddLeaveEntitle.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.debug(AddLeaveEntitle.class);
		Map<String, String> errorMap = new HashMap<String, String>();
		errorMap.clear();
		Properties properties = new Properties();
		try {
		  properties.load(this.getClass().getClassLoader().getResourceAsStream("error.properties"));
		} catch (IOException e) {
		  e.printStackTrace();
		}
		String region = req.getParameter("region");
		String addAnnualLeave = req.getParameter("addAnnualLeave");
		String addSickLeave = req.getParameter("addSickLeave");
		String addMaternityLeave = req.getParameter("addMaternityLeave");
		String addBirthdayLeave = req.getParameter("addBirthdayLeave");
		String addWeddingLeave = req.getParameter("addWeddingLeave");
		String addCompassionateLeave = req.getParameter("addCompassionateLeave");
		String compensationLeaveExp = req.getParameter("compensationLeaveExp");
		
		if(!NumberUtils.isDigits(addAnnualLeave)){
			log.error(""+properties.getProperty("entitled.annual.invalid.numerical"));
			errorMap.put("entitled.annual.invalid.numerical", properties.getProperty("entitled.annual.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(addSickLeave)){
			log.error(""+properties.getProperty("sick.leave.invalid.numerical"));
			errorMap.put("sick.leave.invalid.numerical", properties.getProperty("sick.leave.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(addMaternityLeave)){
			log.error(""+properties.getProperty("maternity.leave.invalid.numerical"));
			errorMap.put("maternity.leave.invalid.numerical", properties.getProperty("maternity.leave.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(addBirthdayLeave)){
			log.error(""+properties.getProperty("birthday.leave.invalid.numerical"));
			errorMap.put("birthday.leave.invalid.numerical", properties.getProperty("birthday.leave.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(addWeddingLeave)){
			log.error(""+properties.getProperty("wedding.leave.invalid.numerical"));
			errorMap.put("wedding.leave.invalid.numerical", properties.getProperty("wedding.leave.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(addCompassionateLeave)){
			log.error(""+properties.getProperty("compassionate.leave.invalid.numerical"));
			errorMap.put("compassionate.leave.invalid.numerical", properties.getProperty("compassionate.leave.invalid.numerical"));
		}
		
		if(!NumberUtils.isDigits(compensationLeaveExp)){
			log.error(""+properties.getProperty("compensation.leave.invalid.numerical"));
			errorMap.put("compensation.leave.invalid.numerical", properties.getProperty("compensation.leave.invalid.numerical"));
		}
		
		/* to check if exist in the database */
		boolean exist = false;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query(LeaveEntitle.class.getSimpleName());
		for (Entity entity : datastore.prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			String tmp = (String)entity.getProperty("region");
			if (tmp == null) {
				exist = false;
			} else if (tmp.equalsIgnoreCase(region)) {
				exist = true;
			}
		}
		
		if (exist == true) {
			try {
				log.error(""+properties.getProperty("region.exist"));
				errorMap.put("region.exist", properties.getProperty("region.exist"));
        		getServletConfig().getServletContext().getRequestDispatcher("/feedback-map.jsp").forward(req, resp);
        		return;
			} catch (ServletException e) {
				log.error("AddLeaveEntitle * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
			
		} else if (exist == false) {
			
			if(!errorMap.isEmpty()){
	        	try {
					req.setAttribute("errorMap", errorMap);
					getServletConfig().getServletContext().getRequestDispatcher("/feedback-map.jsp").forward(req, resp);
					return;
	        	} catch (Exception e1) {
	    			log.error("AddLeaveEntitle validate error: " + e1.getMessage());
	    			e1.printStackTrace();
	    		}
	        }
			
			LeaveEntitleService les = new LeaveEntitleService();
			les.addLeaveEntitle(addAnnualLeave, addSickLeave, addMaternityLeave, addBirthdayLeave,
					addWeddingLeave, addCompassionateLeave, region, compensationLeaveExp);
			try{
				log.error(""+properties.getProperty("save.success"));
				errorMap.put("save.success", properties.getProperty("save.success"));
				getServletConfig().getServletContext().getRequestDispatcher("/feedback-map.jsp").forward(req, resp);
				return;
			} catch (ServletException e) {
				log.error("AddLeaveEntitle * doPost - error 2: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}
}
