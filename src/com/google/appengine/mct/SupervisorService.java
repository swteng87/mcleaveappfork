package com.google.appengine.mct;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.datastore.DataStoreUtil;

public class SupervisorService extends DataStoreUtil {
	
	public List<Supervisor> findSupervisorBy(String columnName, String value, String filterOperator){
		try {
		Supervisor supervisor = new Supervisor();
		List<Supervisor> supervisorList = new ArrayList<Supervisor>();
		Iterable<Entity> entity = listEntities(Supervisor.class.getSimpleName(),  columnName,  value, filterOperator);
		for(Entity result : entity){
			supervisor.setEmailAddress((String)result.getProperty("emailAddress"));
			supervisor.setRegion((String)result.getProperty("region"));
			supervisor.setId(KeyFactory.keyToString(result.getKey()));
			supervisorList.add(supervisor);
			
		}
			return supervisorList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
	
	public Supervisor findSupervisorByColumnName(String columnName, String value){
		try {
			Supervisor supervisor = new Supervisor();
			Entity entity = findEntities(Supervisor.class.getSimpleName(), columnName, value);
			supervisor.setEmailAddress((String)entity.getProperty("emailAddress"));
			supervisor.setRegion((String)entity.getProperty("region"));
			supervisor.setId(KeyFactory.keyToString(entity.getKey()));
			return supervisor;
			} catch (Exception e) {
				e.printStackTrace();
				 return null;
			}
	}
	
	public String addSupervisor(String emailAddress, String region) {
		Entity supervisorEntity = new Entity(Supervisor.class.getSimpleName());
		supervisorEntity.setProperty("emailAddress", emailAddress);
		supervisorEntity.setProperty("region", region);
		getDatastore().put(supervisorEntity);
		return KeyFactory.keyToString(supervisorEntity.getKey());   
	}
	
	
	public List<Supervisor> getSupervisors() {
		Query query = new Query(Supervisor.class.getSimpleName());
		List<Supervisor> results = new ArrayList<Supervisor>();
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			Supervisor supervisor = new Supervisor();
			supervisor.setEmailAddress((String)entity.getProperty("emailAddress"));
			supervisor.setRegion((String)entity.getProperty("region"));
			supervisor.setId(KeyFactory.keyToString(entity.getKey()));
			results.add(supervisor);
		}

		/* Sort supervisors alphabetically */
		Collections.sort(results, new SortSupervisors());
		return results;
	}
	
	
	public void deleteSupervisor(String emailAddress) {;
		Transaction txn = getDatastore().beginTransaction();
		try {
			Query query = new Query(Supervisor.class.getSimpleName());
			for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				String tmp = (String)entity.getProperty("emailAddress");
				if (tmp.equalsIgnoreCase(emailAddress)) {
					getDatastore().delete(entity.getKey());
					txn.commit();
				}
			}
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
	
	public void deleteSupervisor() {
		Query query = new Query(Supervisor.class.getSimpleName());
		for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				getDatastore().delete(entity.getKey());
			}
	}
	
	
	public void updateSupervisor(String emailAddress, String region) throws EntityNotFoundException {
		Transaction txn = getDatastore().beginTransaction();
		try {
			Query query = new Query(Supervisor.class.getSimpleName());
			for (Entity entity : getDatastore().prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
				String tmp = (String)entity.getProperty("emailAddress");
				if (tmp.equalsIgnoreCase(emailAddress)) {
					Entity supervisor = getDatastore().get(entity.getKey());
					supervisor.setProperty("emailAddress", emailAddress);
					supervisor.setProperty("region", region);
					getDatastore().put(supervisor);
					txn.commit();
				}
			}
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}
	}
}
