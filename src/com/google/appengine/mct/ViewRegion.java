package com.google.appengine.mct;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.appengine.datatable.DataTableModel;
import com.google.appengine.datatable.DataTablesUtility;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@SuppressWarnings("serial")
public class ViewRegion extends BaseServlet {

	private static final Logger log = Logger.getLogger(ViewRegion.class);
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		log.debug(ViewRegion.class);
		DataTableModel dataTableModel = DataTablesUtility.getParam(request);
		String sEcho = dataTableModel.sEcho;
		int iTotalRecords = 0; // total number of records (unfiltered)
		int iTotalDisplayRecords = 0; //value will be set when code filters companies by keyword
		JsonArray data = new JsonArray(); //data that will be shown in the table
		
		List<Regions> regionList = new LinkedList<Regions>();
		
		RegionsService rs = new RegionsService();
		
		for(Regions result : rs.getRegions()){
			if((StringUtils.lowerCase(result.getRegion()).contains(dataTableModel.sSearch.toLowerCase())) ||
			   (StringUtils.lowerCase(result.getRegionAbbreviation()).contains(dataTableModel.sSearch.toLowerCase())) ||
			   (StringUtils.lowerCase(result.getRegionSalesOps()).contains(dataTableModel.sSearch.toLowerCase())) ||
			   (StringUtils.lowerCase(result.getRegionCalendarURL()).contains(dataTableModel.sSearch.toLowerCase())) 
					){
				regionList.add(result); // add region that matches given search criterion
			}
		}
		
		iTotalDisplayRecords = regionList.size(); // number of region that match search criterion should be returned
		
		final int sortColumnIndex = dataTableModel.iSortColumnIndex;
		final int sortDirection = dataTableModel.sSortDirection.equals("asc") ? -1 : 1;
		
		Collections.sort(regionList, new Comparator<Regions>(){
			@Override
			public int compare(Regions c1, Regions c2) {	
				switch(sortColumnIndex){
				case 0:
					return c1.getRegion().compareTo(c2.getRegion()) * sortDirection;
				case 1:
					return c1.getRegionAbbreviation().compareTo(c2.getRegionAbbreviation()) * sortDirection;
				case 2:
					return c1.getRegionSalesOps().compareTo(c2.getRegionSalesOps()) * sortDirection;
				case 3:
					return c1.getRegionCalendarURL().compareTo(c2.getRegionCalendarURL()) * sortDirection;
				}
				return 0;
			}
		});
		
		if(regionList.size()< dataTableModel.iDisplayStart + dataTableModel.iDisplayLength) {
			regionList = regionList.subList(dataTableModel.iDisplayStart, regionList.size());
		} else {
			regionList = regionList.subList(dataTableModel.iDisplayStart, dataTableModel.iDisplayStart + dataTableModel.iDisplayLength);
		}
		
		try {
			JsonObject jsonResponse = new JsonObject();			
			jsonResponse.addProperty("sEcho", sEcho);
			jsonResponse.addProperty("iTotalRecords", iTotalRecords);
			jsonResponse.addProperty("iTotalDisplayRecords", iTotalDisplayRecords);
			
			for(Regions region : regionList){
				JsonArray row = new JsonArray();
				row.add(new JsonPrimitive(region.getRegion()));
				row.add(new JsonPrimitive(region.getRegionAbbreviation()));
				row.add(new JsonPrimitive(region.getRegionSalesOps()));
				row.add(new JsonPrimitive(region.getRegionCalendarURL()));
				data.add(row);
			}
			jsonResponse.add("aaData", data);
			
			response.setContentType("application/Json");
			response.getWriter().print(jsonResponse.toString());
			
		} catch (JsonIOException e) {
			e.printStackTrace();
			response.setContentType("text/html");
			response.getWriter().print(e.getMessage());
		}
	
	}

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
	
	}
}
