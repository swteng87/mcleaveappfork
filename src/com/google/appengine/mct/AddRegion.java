package com.google.appengine.mct;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.util.ConstantUtils;

@SuppressWarnings("serial")
public class AddRegion extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(AddRegion.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		log.debug(AddRegion.class);
		String region = req.getParameter("region");
		String regionAbbreviation = req.getParameter("regionAbbreviation");
		String regionCalendarURL = req.getParameter("calendarURL");
		String salesOps = req.getParameter("salesOps");
		
		/* check if exist in the database */
		boolean exist = false;
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query query = new Query(Regions.class.getSimpleName());
		for (Entity entity : datastore.prepare(query).asIterable(FetchOptions.Builder.withDefaults())) {
			String tmp = (String)entity.getProperty("region");
			if (tmp == null) {
				exist = false;
			} else if (tmp.equalsIgnoreCase(region)) {
				exist = true;
			}
		}
		
		if (exist == true) {
			try {
				req.setAttribute("feedback", ConstantUtils.ERROR);
				req.setAttribute("message", "The region already exist in the database.");
        		getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
        		return;
			} catch (ServletException e) {
				log.error("AddRegion * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
		} else if (exist == false) {
			RegionsService rs = new RegionsService();
			rs.addRegions(region, regionAbbreviation, regionCalendarURL, salesOps);
			try{
			req.setAttribute("feedback", ConstantUtils.OK);
			req.setAttribute("message", "Save success.");
			getServletConfig().getServletContext().getRequestDispatcher("/feedback.jsp").forward(req, resp);
    		return;
			} catch (ServletException e) {
				log.error("AddHoliday * doPost - error 2: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}
}
