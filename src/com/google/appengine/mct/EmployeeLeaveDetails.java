package com.google.appengine.mct;

import java.io.Serializable;

public class EmployeeLeaveDetails implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5651952912747478738L;
	private String id;
	private String name;
	private String year;
	private String lastYearBalance;
	private String entitledAnnual;
	private String entitledCompensation;
	private String noPayLeave;
	private String sickLeave;
	private String annualLeave;
	private String compensationLeave;
	private String compassionateLeave;
	private String birthdayLeave;
	private String maternityLeave;
	private String weddingLeave;
	private String balance;
	private String emailAddress;
	private String region;
	private String others;
	
	
	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getYear() {
		return year;
	}
	
	public void setYear(String year) {
		this.year = year;
	}
	
	public String getLastYearBalance() {
		lastYearBalance = lastYearBalance == null ? "0" : lastYearBalance;
		return lastYearBalance;
	}
	
	public void setLastYearBalance(String lastYearBalance) {
		this.lastYearBalance = lastYearBalance;
	}
	
	public String getEntitledAnnual() {
		entitledAnnual = entitledAnnual == null ? "0" : entitledAnnual;
		return entitledAnnual;
	}
	
	public void setEntitledAnnual(String entitledAnnual) {
		this.entitledAnnual = entitledAnnual;
	}
	
	public String getEntitledCompensation() {
		entitledCompensation = entitledCompensation == null ? "0" : entitledCompensation;
		return entitledCompensation;
	}
	
	public void setEntitledCompensation(String entitledCompensation) {
		this.entitledCompensation = entitledCompensation;
	}
	
	public String getNoPayLeave() {
		noPayLeave = noPayLeave == null ? "0" : noPayLeave;
		return noPayLeave;
	}
	
	public void setNoPayLeave(String noPayLeave) {
		this.noPayLeave = noPayLeave;
	}
	
	public String getSickLeave() {
		sickLeave = sickLeave == null ? "0" : sickLeave;
		return sickLeave;
	}
	
	public void setSickLeave(String sickLeave) {
		this.sickLeave = sickLeave;
	}
	
	public String getAnnualLeave() {
		annualLeave = annualLeave == null ? "0" : annualLeave;
		return annualLeave;
	}
	
	public void setAnnualLeave(String annualLeave) {
		this.annualLeave = annualLeave;
	}
	
	public String getCompensationLeave() {
		compensationLeave = compensationLeave == null ? "0" : compensationLeave;
		return compensationLeave;
	}
	
	public void setCompensationLeave(String compensationLeave) {
		this.compensationLeave = compensationLeave;
	}
	
	public String getCompassionateLeave() {
		compassionateLeave = compassionateLeave == null ? "0" :compassionateLeave;
		return compassionateLeave;
	}
	
	public void setCompassionateLeave(String compassionateLeave) {
		this.compassionateLeave = compassionateLeave;
	}
	
	public String getBirthdayLeave() {
		birthdayLeave = birthdayLeave == null ? "0" : birthdayLeave;
		return birthdayLeave;
	}
	
	public void setBirthdayLeave(String birthdayLeave) {
		this.birthdayLeave = birthdayLeave;
	}
	
	public String getMaternityLeave() {
		maternityLeave = maternityLeave == null ? "0" : maternityLeave;
		return maternityLeave;
	}
	
	public void setMaternityLeave(String maternityLeave) {
		this.maternityLeave = maternityLeave;
	}
	
	public String getWeddingLeave() {
		weddingLeave = weddingLeave == null ? "0" : weddingLeave;
		return weddingLeave;
	}
	
	public void setWeddingLeave(String weddingLeave) {
		this.weddingLeave = weddingLeave;
	}
	
	public String getBalance() {
		balance = balance == null ? "0" : balance;
		return balance;
	}
	
	public void setBalance(String balance) {
		this.balance = balance;
	}
	
	public String getOthers() {
		others = others == null ? "0" : others;
		return others;
	}
	
	public void setOthers(String others) {
		this.others = others;
	}
	
}
