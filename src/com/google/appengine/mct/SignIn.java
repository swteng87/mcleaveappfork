package com.google.appengine.mct;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.sso.UserInfo;


@SuppressWarnings("serial")
public class SignIn extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(SignIn.class);

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {

//		String cmd2 = req.getParameter("cmd2");
		String emailAdd = req.getParameter("emailAdd");
		String password = req.getParameter("password");
		boolean validCredentials = false;
		boolean isAdmin = false;
//		String sessionId = req.getSession().getId();
//		MemcacheService memcache = MemcacheServiceFactory.getMemcacheService(null);

		UserService userService = UserServiceFactory.getUserService();
//		User user = userService.getCurrentUser();
		
		UserInfo userInfo = (UserInfo)req.getSession().getAttribute("user");
		AdministratorService as = new AdministratorService();
		EmployeeService emp = new EmployeeService();
		/* Single-Sign-On */
//		if(userInfo != null){
//			emailAdd = userInfo.getEmail();
//			validCredentials = true;
//			log.info("single sign on");
//		}
		
//		if (cmd2!=null && cmd2.equalsIgnoreCase("Google")) {
//			try {
//				if (user != null) {
//					log.debug("Sign In - user: " + user.getEmail());
//					emailAdd = user.getEmail();
//					validCredentials = true;
//				} else {
//					log.debug("Sign In - user is null.");
//					resp.sendRedirect(userService.createLoginURL(req.getRequestURI(), null, "www.google.com/accounts/o8/id", attributesRequest));
//				}
//			} catch (Exception e) {
//				log.error("Sign In - doPost error: " + e.getMessage());
//				e.printStackTrace();
//			}
//		} else {
//			try {
//				DocsService service = new DocsService("wise");
//				service.setUserCredentials(emailAdd, password);
//				URL documentListFeedUrl = new URL("https://docs.google.com/feeds/default/private/full");
//				DocumentListFeed feed = service.getFeed(documentListFeedUrl, DocumentListFeed.class);
//				feed.getEntries();
//				validCredentials = true;
//			} catch (Exception e) {
//				log.error("Error: " + e.getMessage());
//				e.printStackTrace();
//			}
//		}

		

		/* check if exist in the database */
		boolean exist = false;
//		String eAdd = "priscilla.lee@hkmci.com";
		String eAdd = "damon.leong@hkmci.com";
		for (Administrator adm : as.getAdministrators()) {
			if (adm.getEmailAddress().equalsIgnoreCase(eAdd) 
					) {
				exist = true;
			}
		}
		if (exist == false) {
//			as2.addAdministrator("priscilla.lee@hkmci.com");
			as.addAdministrator("damon.leong@hkmci.com");
		}
		
		CompanyPolicyService cs = new CompanyPolicyService();
		CompanyPolicy companyPolicy = new CompanyPolicy();
		companyPolicy = cs.getCompanyPolicy();
		if(StringUtils.isNotBlank(companyPolicy.getContent())){
			req.setAttribute("content", companyPolicy.getContent());
		}
		
		
		try{
//		email and password for developer 
//		if(StringUtils.isNotBlank(emailAdd) && StringUtils.isNotBlank(password)){
//			Employee employee = emp.findEmployeeByColumnName("emailAddress", emailAdd);
//			AuditLog aLog = new AuditLog();
//			aLog.setTime(Misc.getCalendarByLocale(req.getLocale()));
//			aLog.setEmailAddress(emailAdd);
//			aLog.setName(employee.getFullName());
//			AuditActivitiesService auditActivitiesService = new AuditActivitiesService();
//			auditActivitiesService.saveLog(aLog);
//			
//			req.getSession().setAttribute("emailAdd", emailAdd);
//			for (Administrator adm : as.getAdministrators()) {
//				if (emailAdd != null) {
//					if (emailAdd.equalsIgnoreCase(adm.getEmailAddress())) {
//						isAdmin = true;
//						getServletConfig().getServletContext().getRequestDispatcher("/admin-emp-policy.jsp").forward(req, resp);
//						return;
//					}
//				}
//			}
//
//			if (isAdmin == false) {
//				getServletConfig().getServletContext().getRequestDispatcher("/mct-emp-policy.jsp").forward(req, resp);
//				return;
//			}
//		}
		
		/* Single-Sign-On */
		if(userInfo != null){
			emailAdd = userInfo.getEmail();
		Employee employee = emp.findEmployeeByColumnName("emailAddress", emailAdd);
		AuditLog aLog = new AuditLog();
		aLog.setTime(Misc.getCalendarByLocale(req.getLocale()));
		aLog.setEmailAddress(emailAdd);
		aLog.setName(employee.getFullName());
		AuditActivitiesService auditActivitiesService = new AuditActivitiesService();
		auditActivitiesService.saveLog(aLog);
			req.getSession().setAttribute("emailAdd", emailAdd);
			for (Administrator adm : as.getAdministrators()) {
				if (emailAdd != null) {
					if (emailAdd.equalsIgnoreCase(adm.getEmailAddress())) {
						isAdmin = true;
						getServletConfig().getServletContext().getRequestDispatcher("/admin-emp-policy.jsp").forward(req, resp);
//						resp.sendRedirect("/admin-emp-policy.jsp");
					}
				}
			}

			if (isAdmin == false) {
				getServletConfig().getServletContext().getRequestDispatcher("/mct-emp-policy.jsp").forward(req, resp);
//				resp.sendRedirect("/mct-emp-policy.jsp");
			}
			
		} else {
			req.setAttribute("resignUri", userService.createLoginURL(req.getRequestURI()));
			resp.sendRedirect("/sign-in-invalid.jsp");
		}
		
		} catch (ServletException e) {
			log.error("SignIn * doPost - error1a: " + e.getMessage());
			e.printStackTrace();
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}

}