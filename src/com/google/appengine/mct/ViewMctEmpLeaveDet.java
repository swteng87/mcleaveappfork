package com.google.appengine.mct;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.FetchOptions;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.QueryResultList;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.datatable.DataTableModel;
import com.google.appengine.datatable.DataTablesUtility;
import com.google.appengine.util.ConstantUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

@SuppressWarnings("serial")
public class ViewMctEmpLeaveDet extends BaseServlet {
	
	private static final Logger log = Logger.getLogger(ViewMctEmpLeaveDet.class);

	public static StringBuffer empDetailsListTable = new StringBuffer();
	public static StringBuffer selectedEmpDetails = new StringBuffer();
	public static StringBuffer transactionDetails = new StringBuffer();
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		
//		String yearSelected = req.getParameter("cri_year");
		String radioButton = req.getParameter("empDetRad");
		
		EmployeeLeaveDetailsService elds = new EmployeeLeaveDetailsService();
		ApprovedLeaveService als = new ApprovedLeaveService();
		Integer lastYear = 0;
		SimpleDateFormat standardDF = new SimpleDateFormat(ConstantUtils.DATE_FORMAT);
		Integer currentYear = 0;
		Integer nextYear = 0;
		
			EmployeeLeaveDetails eld = elds.findEmployeeLeaveDetailsByValue(Entity.KEY_RESERVED_PROPERTY,radioButton,ConstantUtils.EQUAL);
			req.setAttribute("eld", eld);
			
			String year = eld.getYear();
			currentYear = Integer.parseInt(year);
			lastYear = currentYear - 1;
			nextYear = currentYear + 1;
			
			List<ApprovedLeave> appList = als.getApproveLeaveListByEmail(eld.getEmailAddress());
			List<ApprovedLeave> newAppList = new ArrayList<ApprovedLeave>();
			for(ApprovedLeave approvedLeave : appList){
				Calendar currMonth = Calendar.getInstance(Locale.getDefault());
				if(!ConstantUtils.COMPENSATION_LEAVE_ENTITLEMENT.equals(approvedLeave.getLeaveType())){
					try {
						currMonth.setTime(standardDF.parse(approvedLeave.getStartDate()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(currMonth.get(Calendar.MONTH) > 2 && currMonth.get(Calendar.YEAR) == Integer.parseInt(year)){
						ApprovedLeave appLeave = new ApprovedLeave();
						appLeave.setTime(approvedLeave.getTime());
						appLeave.setEmailAdd(approvedLeave.getEmailAdd());
						appLeave.setNumOfDays(approvedLeave.getNumOfDays());
						appLeave.setStartDate(approvedLeave.getStartDate());
						appLeave.setEndDate(approvedLeave.getEndDate());
						appLeave.setLeaveType(approvedLeave.getLeaveType());
						appLeave.setSupervisor(approvedLeave.getSupervisor());
						appLeave.setRemark(approvedLeave.getRemark());
						appLeave.setAttachmentUrl(approvedLeave.getAttachmentUrl());
						appLeave.setRegion(approvedLeave.getRegion());
						appLeave.setChangeType(approvedLeave.getChangeType());
						appLeave.setId(approvedLeave.getId());
						newAppList.add(appLeave);
					}
				}
				else{
					try {
						currMonth.setTime(standardDF.parse(approvedLeave.getTime()));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if(currMonth.get(Calendar.MONTH) > 2 && currMonth.get(Calendar.YEAR) == Integer.parseInt(year)){
						ApprovedLeave appLeave = new ApprovedLeave();
						appLeave.setTime(approvedLeave.getTime());
						appLeave.setEmailAdd(approvedLeave.getEmailAdd());
						appLeave.setNumOfDays(approvedLeave.getNumOfDays());
						appLeave.setStartDate(approvedLeave.getStartDate());
						appLeave.setEndDate(approvedLeave.getEndDate());
						appLeave.setLeaveType(approvedLeave.getLeaveType());
						appLeave.setSupervisor(approvedLeave.getSupervisor());
						appLeave.setRemark(approvedLeave.getRemark());
						appLeave.setAttachmentUrl(approvedLeave.getAttachmentUrl());
						appLeave.setRegion(approvedLeave.getRegion());
						appLeave.setChangeType(approvedLeave.getChangeType());
						appLeave.setId(approvedLeave.getId());
						newAppList.add(appLeave);
					}
					
				}
			}
			req.setAttribute("appList", newAppList);
			
			Double entitledTotal = 0.0;
			Double others = 0.0;
			String lastYearBalance = eld.getLastYearBalance() == null ? "0" : eld.getLastYearBalance();
			String entitledAnnual= eld.getEntitledAnnual() == null ? "0" : eld.getEntitledAnnual();
			String entitledCompensation = eld.getEntitledCompensation()  == null ? "0" : eld.getEntitledCompensation();
			entitledTotal = (Double.parseDouble(lastYearBalance)) + 
					(Double.parseDouble(entitledAnnual)) + 
					(Double.parseDouble(entitledCompensation));
			
			String compassionateLeave = eld.getCompassionateLeave() == null ? "0" : eld.getCompassionateLeave();
			String birthdayLeave = eld.getBirthdayLeave() == null ? "0" : eld.getBirthdayLeave();
			String maternityLeave = eld.getMaternityLeave() == null ? "0" : eld.getMaternityLeave();
			String weddingLeave = eld.getWeddingLeave()  == null ? "0" : eld.getWeddingLeave(); 
			
			others = (Double.parseDouble(compassionateLeave)) + 
					(Double.parseDouble(birthdayLeave)) + 
					(Double.parseDouble(maternityLeave)) + 
					(Double.parseDouble(weddingLeave));
			
			req.setAttribute("yearSelected", year);
			req.setAttribute("lastYear", lastYear.toString());
			req.setAttribute("entitledTotal", entitledTotal.toString());
			req.setAttribute("others", others.toString());
			req.setAttribute("currentYear", currentYear.toString());
			req.setAttribute("nextYear", nextYear.toString());
			req.setAttribute("fullName", eld.getName());
			
			try {
				getServletConfig().getServletContext().getRequestDispatcher("/mct-view-emp-leave-details-action.jsp").forward(req, resp);
				return;
			} catch (ServletException e) {
				log.error("ViewEmpLeaveDetails * doPost - error: " + e.getMessage());
				e.printStackTrace();
			}
		
	}

	public void doPostt(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		empDetailsListTable.setLength(0);
		selectedEmpDetails.setLength(0);
		transactionDetails.setLength(0);
		int bal = 0;
		int index = 0;
		int yearInt = 0;
		int monthInt = 0;
		int firstInd = 0;
		int lastInd = 0;
		String monthStr = "";
		String yearStr = "";
		String thisYear = "";
		String nextYear = "";
		String lastYear = "";
		String yearSelected = req.getParameter("cri_year");

		int ind = 0;
		int digitYear = 0;
		int secondYear = 0;
		double balance = 0.0;
		digitYear = Integer.parseInt(yearSelected);
		secondYear = digitYear + 1;

		UserService userService = UserServiceFactory.getUserService();
		User user = userService.getCurrentUser();
		String emailAddress = (String)req.getSession().getAttribute("emailAdd");
		
		EmployeeLeaveDetailsService elds = new EmployeeLeaveDetailsService();
		for (EmployeeLeaveDetails eld : elds.getEmployeeLeaveDetails()) {
			
			if(user!=null){
				emailAddress = user.getEmail();
			}
				if (emailAddress.equalsIgnoreCase(eld.getEmailAddress())) {
					if (eld.getYear().equalsIgnoreCase(yearSelected)) {
						selectedEmpDetails.append("<tr>");
						selectedEmpDetails.append("<td width=\"190px\">Name</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getName() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Year</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getYear() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Last Year's Balance</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getLastYearBalance() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Entitled Annual</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getEntitledAnnual() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Entitled Compensation</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getEntitledCompensation() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Annual Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getAnnualLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Sick Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getSickLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Birthday Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getBirthdayLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>No Pay Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getNoPayLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Compensation Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getCompensationLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Compassionate Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getCompassionateLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Maternity Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getMaternityLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Wedding Leave</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getWeddingLeave() + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("<td>Balance</td>");
						selectedEmpDetails.append("<td width=\"540px\"><div class=\"textbox\">" + 
								"<input type=\"text\" value=\"" + eld.getBalance() + " Days" + "\" readOnly/>" + "</div></td>");
						selectedEmpDetails.append("</tr><tr>");
						selectedEmpDetails.append("</tr><tr><td colspan=\"2\"><hr></td></tr>");

						transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
						transactionDetails.append("<td align=\"left\">");
						transactionDetails.append("Balance b/d ending 31 Mar " + digitYear);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getLastYearBalance());
						transactionDetails.append("</td>");
						balance = Double.parseDouble(eld.getLastYearBalance());
						transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
						transactionDetails.append("<td>" + balance + "</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
						transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
						transactionDetails.append("<td align=\"left\">");
						transactionDetails.append("From 1 Apr " + digitYear + " to 31 Mar " + secondYear);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getEntitledAnnual());
						transactionDetails.append("</td>");
						balance = balance + Double.parseDouble(eld.getEntitledAnnual());
						transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
						transactionDetails.append("<td>" + balance + "</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
						ApprovedLeaveService als = new ApprovedLeaveService();
						for (ApprovedLeave al : als.getApprovedLeave()) {
							if (al.getEmailAdd().equalsIgnoreCase(eld.getName())) {
								firstInd = al.getStartDate().indexOf("-");
								lastInd = al.getStartDate().lastIndexOf("-");
								monthStr = al.getStartDate().substring(firstInd+1, lastInd);
								yearStr = al.getStartDate().substring(lastInd+1, al.getStartDate().length());
								monthInt = Integer.parseInt(monthStr);
								thisYear = yearSelected;
								yearInt = Integer.parseInt(yearStr);
								lastYear = Integer.toString(yearInt - 1);
								nextYear = Integer.toBinaryString(yearInt + 1);
								if (monthInt <= 3) {
									if (yearSelected.equalsIgnoreCase(lastYear)) {
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave Entitlement")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											balance = balance + Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\">" + al.getRemark() + "</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Sick Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("No Pay Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Annual Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compassionate Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Compassionate Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Birthday Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Birthday Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Maternity Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Maternity Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Wedding Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Wedding Leave</td>");
											transactionDetails.append("</tr>");
										}
									}
								}
								else {
									if (al.getStartDate().contains(yearSelected)) {
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave Entitlement")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											balance = balance + Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\">" + al.getRemark() + "</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Sick Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("No Pay Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Annual Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td><td></td>");
											transactionDetails.append("<td>"+ balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compensation Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											balance = balance - Double.parseDouble(al.getNumOfDays());
											transactionDetails.append("<td></td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\"></td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Compassionate Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Compassionate Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Birthday Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Birthday Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Maternity Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Maternity Leave</td>");
											transactionDetails.append("</tr>");
										}
										if (al.getLeaveType().equalsIgnoreCase("Wedding Leave")) {
											transactionDetails.append("<tr bgcolor=\"#F0F0F0\" align=\"center\">");
											transactionDetails.append("<td align=\"left\">");
											transactionDetails.append(al.getStartDate() + " - " + al.getEndDate());
											transactionDetails.append("</td>");
											transactionDetails.append("<td></td><td></td><td></td><td></td><td></td>");
											transactionDetails.append("<td>" + al.getNumOfDays() + "</td>");
											transactionDetails.append("<td>" + balance + "</td>");
											transactionDetails.append("<td align=\"left\">Wedding Leave</td>");
											transactionDetails.append("</tr>");
										}
									}
								}
							}
						}
						transactionDetails.append("<tr bgcolor=\"#FFF8C6\" align=\"center\">");
						transactionDetails.append("<td align=\"right\">Total</td>");
						transactionDetails.append("<td>");
						double entitledTotal = 0.0;
						double others = 0.0;
						entitledTotal = (Double.parseDouble(eld.getLastYearBalance())) + (Double.parseDouble(eld.getEntitledAnnual())) + (Double.parseDouble(eld.getEntitledCompensation()));
						others = (Double.parseDouble(eld.getCompassionateLeave())) + (Double.parseDouble(eld.getBirthdayLeave())) + (Double.parseDouble(eld.getMaternityLeave())) + (Double.parseDouble(eld.getWeddingLeave()));
						transactionDetails.append(entitledTotal);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getNoPayLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getSickLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getAnnualLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getCompensationLeave());
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(others);
						transactionDetails.append("</td>");
						transactionDetails.append("<td>");
						transactionDetails.append(eld.getBalance());
						transactionDetails.append("</td>");
						transactionDetails.append("<td></td>");
						transactionDetails.append("</tr>");
					}
				}
			
		}
		try {
			getServletConfig().getServletContext().getRequestDispatcher("/mct-view-emp-leave-details-action.jsp").forward(req, resp);
			return;
		} catch (ServletException e) {
			log.error("ViewMctEmpLeaveDet * doPost - error: " + e.getMessage());
			e.printStackTrace();
		}
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		log.debug(ViewMctEmpLeaveDet.class);
		DataTableModel dataTableModel = DataTablesUtility.getParam(request);
		String sEcho = dataTableModel.sEcho;
		int iTotalRecords = 0; // total number of records (unfiltered)
		int iTotalDisplayRecords = 0; //value will be set when code filters companies by keyword
		JsonArray data = new JsonArray(); //data that will be shown in the table
		String emailAddress = (String)request.getSession().getAttribute("emailAdd");
		
		List<EmployeeLeaveDetails> employeeLeaveDetailsList = new LinkedList<EmployeeLeaveDetails>();
		List<EmployeeLeaveDetails> entityList = new LinkedList<EmployeeLeaveDetails>();
//		if(StringUtils.isBlank(dataTableModel.sSearch)){
			
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Query q = new Query(EmployeeLeaveDetails.class.getSimpleName());
		
		Filter emailFilter = new FilterPredicate("emailAddress",
                FilterOperator.EQUAL,
                emailAddress);
				
		q.setFilter(emailFilter);
		
		// PreparedQuery contains the methods for fetching query results from the datastore
		PreparedQuery pq = datastore.prepare(q);
		
		iTotalRecords = pq.countEntities(FetchOptions.Builder.withDefaults());
		
		QueryResultList<Entity> results =  pq.asQueryResultList(FetchOptions.Builder.withDefaults());
		for(Entity result : results){
			EmployeeLeaveDetails employeeLeaveDetails = new EmployeeLeaveDetails();
			employeeLeaveDetails.setId(KeyFactory.keyToString(result.getKey()));
			employeeLeaveDetails.setName((String)result.getProperty("name"));
			employeeLeaveDetails.setEmailAddress((String)result.getProperty("emailAddress"));
			employeeLeaveDetails.setYear((String)result.getProperty("year"));
			entityList.add(employeeLeaveDetails);
		}
		
		for(EmployeeLeaveDetails result : entityList){
			if(
			   (StringUtils.lowerCase(result.getYear()).contains(dataTableModel.sSearch.toLowerCase()))){
				employeeLeaveDetailsList.add(result); // add employee that matches given search criterion
			}
		}
		
		iTotalDisplayRecords = employeeLeaveDetailsList.size(); // number of employee that match search criterion should be returned
		
		final int sortColumnIndex = dataTableModel.iSortColumnIndex;
		final int sortDirection = dataTableModel.sSortDirection.equals("asc") ? -1 : 1;
		
		Collections.sort(employeeLeaveDetailsList, new Comparator<EmployeeLeaveDetails>(){
			@Override
			public int compare(EmployeeLeaveDetails c1, EmployeeLeaveDetails c2) {	
				switch(sortColumnIndex){
				case 0:
					return c1.getYear().compareTo(c2.getYear()) * sortDirection;
				}
				return 0;
			}
		});
		
		if(employeeLeaveDetailsList.size()< dataTableModel.iDisplayStart + dataTableModel.iDisplayLength) {
			employeeLeaveDetailsList = employeeLeaveDetailsList.subList(dataTableModel.iDisplayStart, employeeLeaveDetailsList.size());
		} else {
			employeeLeaveDetailsList = employeeLeaveDetailsList.subList(dataTableModel.iDisplayStart, dataTableModel.iDisplayStart + dataTableModel.iDisplayLength);
		}
		
		try {
			JsonObject jsonResponse = new JsonObject();			
			jsonResponse.addProperty("sEcho", sEcho);
			jsonResponse.addProperty("iTotalRecords", iTotalRecords);
			jsonResponse.addProperty("iTotalDisplayRecords", iTotalDisplayRecords);
			
			for(EmployeeLeaveDetails employeeLeaveDetails : employeeLeaveDetailsList){
				JsonArray row = new JsonArray();
				row.add(new JsonPrimitive("<input type=\"radio\" name=\"" + "empDetRad" + "\"" + " value=\"" + employeeLeaveDetails.getId() + "\"" + "onClick=\"javascript:cmd_parm();\"/>"));
				row.add(new JsonPrimitive(employeeLeaveDetails.getYear()));
				data.add(row);
			}
			jsonResponse.add("aaData", data);
			
			response.setContentType("application/Json");
			response.getWriter().print(jsonResponse.toString());
			
		} catch (JsonIOException e) {
			e.printStackTrace();
			response.setContentType("text/html");
			response.getWriter().print(e.getMessage());
		}
	}


	public String empDetailsListTableSb() {
		if (empDetailsListTable.length() == 0) {
			empDetailsListTable.append("<tr bgcolor=\"#FFF8C6\">");
			empDetailsListTable.append("<td colspan=\"4\">" + " " + "</td>");
			empDetailsListTable.append("</tr>");
			empDetailsListTable.append("<tr bgcolor=\"#F0F0F0\">");
			empDetailsListTable.append("<td colspan=\"4\">" + "There are no records in the database for this region." + "</td>");
			empDetailsListTable.append("</tr>");
		}
		return empDetailsListTable.toString();
	}


	public String selectedEmpDetailsSb() {
		if (selectedEmpDetails.length() == 0) {
			selectedEmpDetails.append("<tr bgcolor=\"#FFF8C6\">");
			selectedEmpDetails.append("<td colspan=\"4\">" + " " + "</td>");
			selectedEmpDetails.append("</tr>");
			selectedEmpDetails.append("<tr bgcolor=\"#F0F0F0\">");
			selectedEmpDetails.append("<td colspan=\"4\">" + "There are no records in the database for this year." + "</td>");
			selectedEmpDetails.append("</tr>");
		}
		return selectedEmpDetails.toString();
	}


	public String transactionDetailsSb() {
		return transactionDetails.toString();
	}
}
